package br.com.itau.instrumentos.apicadastro.infra.dataprovider.jpa.mapper;

import java.util.Objects;

import br.com.itau.instrumentos.apicadastro.core.entity.Ativo;
import br.com.itau.instrumentos.apicadastro.infra.dataprovider.jpa.entity.JpaAtivoEntity;

public interface JpaAtivoMapper {

    static JpaAtivoEntity toJpa(Ativo ativo) {
        if (Objects.nonNull(ativo)) {
            JpaAtivoEntity entity = new JpaAtivoEntity();

            entity.setId(ativo.getId());

            entity.setDataHoraCriacao(ativo.getDataHoraCriacao());

            entity.setCodigo(ativo.getCodigo());

            entity.setDescricao(ativo.getDescricao());

            entity.setEmissor(ativo.getEmissor());

            return entity;
        } else {
            return null;
        }
    }

    static Ativo toModel(JpaAtivoEntity jpaEntity) {
        if (Objects.nonNull(jpaEntity)) {
            Ativo ativo = new Ativo(jpaEntity.getId());

            ativo.setCodigo(jpaEntity.getCodigo());

            ativo.setDescricao(jpaEntity.getDescricao());

            ativo.setEmissor(jpaEntity.getEmissor());

            return ativo;
        } else {
            return null;
        }
    }
}