package br.com.itau.instrumentos.apicadastro.infra.dataprovider.jpa.repository;

import br.com.itau.instrumentos.apicadastro.infra.dataprovider.jpa.entity.JpaAtivoEntity;
import org.springframework.data.jpa.repository.JpaRepository;

import java.util.List;

public interface JpaAtivoRepository extends JpaRepository<JpaAtivoEntity, String> {

    List<JpaAtivoEntity> findByCodigo(String codigo);

}