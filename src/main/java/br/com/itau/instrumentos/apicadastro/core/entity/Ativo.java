package br.com.itau.instrumentos.apicadastro.core.entity;

import br.com.itau.instrumentos.base.entity.BaseEntity;

public class Ativo extends BaseEntity {
    private String codigo;

    private String descricao;

    private String emissor;

    public Ativo(){

    }

    public Ativo(String id){
        this.id = id;
    }

    public String getCodigo() {
        return codigo;
    }

    public void setCodigo(String codigo) {
        this.codigo = codigo;
    }

    public String getDescricao() {
        return descricao;
    }

    public void setDescricao(String descricao) {
        this.descricao = descricao;
    }

    public String getEmissor() {
        return emissor;
    }

    public void setEmissor(String emissor) {
        this.emissor = emissor;
    }
}
