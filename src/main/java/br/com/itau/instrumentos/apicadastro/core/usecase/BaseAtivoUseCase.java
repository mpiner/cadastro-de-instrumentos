package br.com.itau.instrumentos.apicadastro.core.usecase;

import br.com.itau.instrumentos.apicadastro.core.usecase.dto.AtivoRequest;
import br.com.itau.instrumentos.apicadastro.core.usecase.dto.AtivoResponse;
import br.com.itau.instrumentos.base.dto.response.ListaErroEnum;
import br.com.itau.instrumentos.base.dto.response.ResponseDataErro;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

public abstract class BaseAtivoUseCase {

    protected Logger logger = LoggerFactory.getLogger(this.getClass());

    protected void validarCamposObrigatorios(AtivoRequest input, AtivoResponse response) {

        if (input.getCodigo().isBlank()) {
            String msg = "O campo código é obrigatório";
            logger.error(msg);
            response.getResponse().adicionarErro(new ResponseDataErro(msg, ListaErroEnum.CAMPOS_OBRIGATORIOS));
        }

        if (input.getDescricao().isBlank()) {
            String msg = "O campo descrição é obrigatório";
            logger.error(msg);
            response.getResponse().adicionarErro(new ResponseDataErro(msg, ListaErroEnum.CAMPOS_OBRIGATORIOS));
        }

        if (input.getEmissor().isBlank()) {
            String msg = "O campo emissor é obrigatório";
            logger.error(msg);
            response.getResponse().adicionarErro(new ResponseDataErro(msg, ListaErroEnum.CAMPOS_OBRIGATORIOS));
        }
    }
}
