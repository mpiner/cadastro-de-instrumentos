package br.com.itau.instrumentos.apicadastro.infra.configuration;

import br.com.itau.instrumentos.apicadastro.core.usecase.gateway.ConfiguracaoGateway;
import org.springframework.context.annotation.Configuration;

@Configuration
public class Configuracao implements ConfiguracaoGateway {

    @Override
    public Integer getQuantidadeRegistrosPorPagina() {
        return 2;
    }
}
