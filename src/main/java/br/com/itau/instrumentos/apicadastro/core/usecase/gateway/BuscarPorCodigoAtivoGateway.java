package br.com.itau.instrumentos.apicadastro.core.usecase.gateway;

import br.com.itau.instrumentos.apicadastro.core.entity.Ativo;

import java.util.Optional;

public interface BuscarPorCodigoAtivoGateway {

    Optional<Ativo> buscarPorCodigoAtivo(String codigo);

}
