package br.com.itau.instrumentos.apicadastro.core.usecase;

import br.com.itau.instrumentos.apicadastro.core.usecase.dto.PaginadoAtivoResponse;
import br.com.itau.instrumentos.apicadastro.core.usecase.gateway.BuscarAtivosGateway;
import br.com.itau.instrumentos.apicadastro.core.usecase.gateway.ConfiguracaoGateway;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.stereotype.Service;

import java.util.Objects;

@Service
public class BuscarAtivosUseCaseImpl implements BuscarAtivosUseCase {

    private final BuscarAtivosGateway buscarAtivosGateway;

    private final ConfiguracaoGateway configuracaoGateway;

    private Logger log = LoggerFactory.getLogger(BuscarAtivosUseCaseImpl.class);


    public BuscarAtivosUseCaseImpl(BuscarAtivosGateway buscarAtivosGateway, ConfiguracaoGateway configuracaoGateway) {
        this.buscarAtivosGateway = buscarAtivosGateway;

        this.configuracaoGateway = configuracaoGateway;
    }

    @Override
    public PaginadoAtivoResponse executar(Integer pagina) {
        log.info("Buscando ativos da página " + pagina + ". Qtd Registros máximo = " + configuracaoGateway.getQuantidadeRegistrosPorPagina());

        if (Objects.isNull(pagina) || pagina < 0) {
            log.warn("Estamos setando a página = 0, pois a página passada é igual a = " + pagina);

            pagina = 0;
        }

        PaginadoAtivoResponse response = buscarAtivosGateway.buscarAtivos(++pagina, configuracaoGateway.getQuantidadeRegistrosPorPagina());

        log.info("Foram encontrados [" + response.getQtdRegistrosDaPagina() + "] registros na página " + pagina);

        log.info("Foram encontrados [" + response.getQtdRegistrosTotais() + "] registros totais");

        return response;
    }
}
