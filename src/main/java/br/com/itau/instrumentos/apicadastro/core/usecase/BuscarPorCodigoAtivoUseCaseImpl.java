package br.com.itau.instrumentos.apicadastro.core.usecase;

import br.com.itau.instrumentos.apicadastro.core.entity.Ativo;
import br.com.itau.instrumentos.apicadastro.core.usecase.dto.AtivoResponse;
import br.com.itau.instrumentos.apicadastro.core.usecase.gateway.BuscarPorCodigoAtivoGateway;
import br.com.itau.instrumentos.base.dto.response.ListaErroEnum;
import br.com.itau.instrumentos.base.dto.response.ResponseDataErro;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.stereotype.Service;

import java.util.Optional;

@Service
public class BuscarPorCodigoAtivoUseCaseImpl implements BuscarPorCodigoAtivoUseCase {

    private final BuscarPorCodigoAtivoGateway buscarPorCodigoAtivoGateway;
    private Logger log = LoggerFactory.getLogger(BuscarPorCodigoAtivoUseCaseImpl.class);

    public BuscarPorCodigoAtivoUseCaseImpl(BuscarPorCodigoAtivoGateway buscarPorCodigoAtivoGateway) {
        this.buscarPorCodigoAtivoGateway = buscarPorCodigoAtivoGateway;
    }

    @Override
    public AtivoResponse executar(String codigo) {
        log.info("Buscando ativo pelo código " + codigo);

        AtivoResponse response = new AtivoResponse();

        response.setCodigo(codigo);

        validarCamposObrigatorios(codigo, response);

        if (!response.getResponse().getErros().isEmpty()) {
            return response;
        }

        Optional<Ativo> opAtivo = buscarPorCodigoAtivoGateway.buscarPorCodigoAtivo(codigo);

        if (opAtivo.isPresent()) {
            log.debug("Ativo encontrado com código " + codigo);

            response.setDescricao(opAtivo.get().getDescricao());

            response.setId(opAtivo.get().getId());

            response.setEmissor(opAtivo.get().getEmissor());
        } else {
            log.debug("Ativo não encontrado com código " + codigo);

            response.getResponse().adicionarErro(new ResponseDataErro("Ativo não encontrado com código " + codigo,
                    ListaErroEnum.ENTIDADE_NAO_ENCONTRADA));
        }

        return response;
    }

    private void validarCamposObrigatorios(String codigo, AtivoResponse response) {
        log.debug("Validando campos obrigatórios");

        log.debug("codigo = " + codigo);

        if (codigo.isBlank()) {
            response.getResponse().adicionarErro(
                    new ResponseDataErro("Campo código é obrigatório", ListaErroEnum.CAMPOS_OBRIGATORIOS));
        }
    }
}
