package br.com.itau.instrumentos.apicadastro.core.usecase.gateway;

import br.com.itau.instrumentos.apicadastro.core.usecase.dto.PaginadoAtivoResponse;

public interface BuscarAtivosGateway {

    PaginadoAtivoResponse buscarAtivos(Integer pagina, Integer qtdPorPagina);

}
