package br.com.itau.instrumentos.base.dto.response;

public abstract class BaseResponse {
    private ResponseData response = new ResponseData();

    public ResponseData getResponse() {
        return response;
    }

    public void setResponse(ResponseData response) {
        this.response = response;
    }
}