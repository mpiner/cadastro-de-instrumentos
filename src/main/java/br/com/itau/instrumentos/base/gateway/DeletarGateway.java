package br.com.itau.instrumentos.base.gateway;

public interface DeletarGateway<ID> {

    Boolean deletar(ID id);

}
