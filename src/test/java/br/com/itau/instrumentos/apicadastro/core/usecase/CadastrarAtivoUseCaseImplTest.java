package br.com.itau.instrumentos.apicadastro.core.usecase;

import br.com.itau.instrumentos.apicadastro.core.entity.Ativo;
import br.com.itau.instrumentos.apicadastro.core.usecase.dto.AtivoRequest;
import br.com.itau.instrumentos.apicadastro.core.usecase.dto.AtivoResponse;
import br.com.itau.instrumentos.apicadastro.core.usecase.gateway.BuscarPorCodigoAtivoGateway;
import br.com.itau.instrumentos.apicadastro.template.AtivoRequestTemplateLoader;
import br.com.itau.instrumentos.base.dto.response.ListaErroEnum;
import br.com.itau.instrumentos.base.gateway.SalvarGateway;
import br.com.six2six.fixturefactory.Fixture;
import br.com.six2six.fixturefactory.loader.FixtureFactoryLoader;
import org.junit.Assert;
import org.junit.Before;
import org.junit.BeforeClass;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.Mockito;
import org.mockito.junit.MockitoJUnitRunner;

import java.util.Optional;

@RunWith(MockitoJUnitRunner.class)
public class CadastrarAtivoUseCaseImplTest {

    @InjectMocks
    private CadastrarAtivoUseCaseImpl usecase;

    @Mock
    private SalvarGateway<Ativo> gateway;

    @Mock
    private BuscarPorCodigoAtivoGateway buscarPorCodigoAtivoGateway;

    @Before
    public void before() {
        Mockito.when(gateway.salvar(Mockito.any())).thenAnswer(i -> i.getArgument(0));
    }

    @BeforeClass
    public static void beforeClass() {
        FixtureFactoryLoader.loadTemplates("br.com.itau.instrumentos.apicadastro.template");
    }

    @Test
    public void deve_salvar_ativo() {
        AtivoRequest request = Fixture.from(AtivoRequest.class).gimme(AtivoRequestTemplateLoader.RANDOM);

        AtivoResponse response = usecase.executar(request);

        Assert.assertNotNull(response);
        Assert.assertNotNull(response.getId());
        Assert.assertEquals(request.getCodigo(), response.getCodigo());
        Assert.assertEquals(request.getDescricao(), response.getDescricao());
        Assert.assertEquals(request.getEmissor(), response.getEmissor());
    }

    @Test
    public void nao_deve_salvar_ativo_duplicado() {
        AtivoRequest request = Fixture.from(AtivoRequest.class).gimme(AtivoRequestTemplateLoader.PETR4);

        Mockito.when(buscarPorCodigoAtivoGateway.buscarPorCodigoAtivo(request.getCodigo())).thenReturn(Optional.of(new Ativo()));

        AtivoResponse response = usecase.executar(request);

        Assert.assertNotNull(response);
        Assert.assertNull(response.getId());

        Assert.assertNotNull(response.getResponse());
        Assert.assertEquals(1, response.getResponse().getErros().size());
        Assert.assertEquals(ListaErroEnum.DUPLICIDADE, response.getResponse().getErros().get(0).getTipo());
        Assert.assertEquals(request.getCodigo(), response.getCodigo());
        Assert.assertEquals(request.getDescricao(), response.getDescricao());
        Assert.assertEquals(request.getEmissor(), response.getEmissor());
    }

    @Test
    public void nao_deve_salvar_ativo_campos_ausentes() {
        AtivoRequest request = new AtivoRequest();
        request.setCodigo("          ");

        AtivoResponse response = usecase.executar(request);

        Assert.assertNotNull(response);
        Assert.assertNull(response.getId());

        Assert.assertNotNull(response.getResponse());
        Assert.assertEquals(3, response.getResponse().getErros().size());

        Assert.assertEquals(ListaErroEnum.CAMPOS_OBRIGATORIOS, response.getResponse().getErros().get(0).getTipo());
        Assert.assertEquals(ListaErroEnum.CAMPOS_OBRIGATORIOS, response.getResponse().getErros().get(1).getTipo());
        Assert.assertEquals(ListaErroEnum.CAMPOS_OBRIGATORIOS, response.getResponse().getErros().get(2).getTipo());
    }
}