package br.com.itau.instrumentos.apicadastro.core.usecase;

import br.com.itau.instrumentos.apicadastro.core.entity.Ativo;
import br.com.itau.instrumentos.apicadastro.core.usecase.dto.AtivoRequest;
import br.com.itau.instrumentos.apicadastro.core.usecase.dto.AtivoResponse;
import br.com.itau.instrumentos.apicadastro.core.usecase.gateway.BuscarPorCodigoAtivoGateway;
import br.com.itau.instrumentos.apicadastro.template.AtivoRequestTemplateLoader;
import br.com.itau.instrumentos.apicadastro.template.AtivoTemplateLoader;
import br.com.itau.instrumentos.base.dto.response.ListaErroEnum;
import br.com.itau.instrumentos.base.gateway.SalvarGateway;
import br.com.six2six.fixturefactory.Fixture;
import br.com.six2six.fixturefactory.loader.FixtureFactoryLoader;
import org.junit.Assert;
import org.junit.Before;
import org.junit.BeforeClass;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.Mockito;
import org.mockito.junit.MockitoJUnitRunner;

import java.util.Optional;

@RunWith(MockitoJUnitRunner.class)
public class AtualizarAtivoUseCaseImplTest {

    @InjectMocks
    private AtualizarAtivoUseCaseImpl usecase;

    @Mock
    private SalvarGateway<Ativo> gateway;

    @Mock
    private BuscarPorCodigoAtivoGateway buscarPorCodigoAtivoGateway;

    @Before
    public void before() {
        Mockito.when(gateway.salvar(Mockito.any())).thenAnswer(i -> i.getArgument(0));
    }

    @BeforeClass
    public static void beforeClass() {
        FixtureFactoryLoader.loadTemplates("br.com.itau.instrumentos.apicadastro.template");
    }

    @Test
    public void deve_atualizar_ativo() {
        Ativo ativo = Fixture.from(Ativo.class).gimme(AtivoTemplateLoader.PETR4);

        String descricaoAntiga = ativo.getDescricao();

        String emissorAntigo = ativo.getEmissor();

        Mockito.when(buscarPorCodigoAtivoGateway.buscarPorCodigoAtivo(ativo.getCodigo())).thenReturn(Optional.of(ativo));

        AtivoRequest request = new AtivoRequest();
        request.setCodigo(ativo.getCodigo());
        request.setDescricao("ATUALIZEI A DESCRICAO");
        request.setEmissor("ATUALIZEI O EMISSOR");

        AtivoResponse response = usecase.executar(request);

        Assert.assertNotNull(response);
        Assert.assertNotNull(response.getId());
        Assert.assertEquals(request.getCodigo(), response.getCodigo());
        Assert.assertNotEquals(descricaoAntiga, response.getDescricao());
        Assert.assertNotEquals(emissorAntigo, response.getEmissor());
        Assert.assertEquals("ATUALIZEI A DESCRICAO", response.getDescricao());
        Assert.assertEquals("ATUALIZEI O EMISSOR", response.getEmissor());
    }

    @Test
    public void nao_deve_atualizar_ativo_campos_ausentes() {
        AtivoRequest request = new AtivoRequest();
        request.setCodigo("          ");
        request.setDescricao("");
        request.setEmissor("");

        AtivoResponse response = usecase.executar(request);

        Assert.assertNotNull(response);
        Assert.assertNull(response.getId());

        Assert.assertNotNull(response.getResponse());
        Assert.assertEquals(3, response.getResponse().getErros().size());

        Assert.assertEquals(ListaErroEnum.CAMPOS_OBRIGATORIOS, response.getResponse().getErros().get(0).getTipo());
        Assert.assertEquals(ListaErroEnum.CAMPOS_OBRIGATORIOS, response.getResponse().getErros().get(1).getTipo());
        Assert.assertEquals(ListaErroEnum.CAMPOS_OBRIGATORIOS, response.getResponse().getErros().get(2).getTipo());

    }

    @Test
    public void nao_deve_atualizar_ativo_inexistente() {
        Mockito.when(buscarPorCodigoAtivoGateway.buscarPorCodigoAtivo(Mockito.anyString())).thenReturn(Optional.empty());

        AtivoRequest request = Fixture.from(AtivoRequest.class).gimme(AtivoRequestTemplateLoader.PETR4);

        AtivoResponse response = usecase.executar(request);

        Assert.assertNotNull(response);
        Assert.assertNull(response.getId());
        Assert.assertEquals(request.getCodigo(), response.getCodigo());
        Assert.assertEquals(request.getDescricao(), response.getDescricao());
        Assert.assertEquals(request.getEmissor(), response.getEmissor());

        Assert.assertNotNull(response.getResponse());
        Assert.assertEquals(1, response.getResponse().getErros().size());
        Assert.assertEquals(ListaErroEnum.ENTIDADE_NAO_ENCONTRADA, response.getResponse().getErros().get(0).getTipo());
    }
}
