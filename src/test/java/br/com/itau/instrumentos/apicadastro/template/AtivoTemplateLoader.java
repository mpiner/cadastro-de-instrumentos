package br.com.itau.instrumentos.apicadastro.template;

import br.com.itau.instrumentos.apicadastro.core.entity.Ativo;
import br.com.six2six.fixturefactory.Fixture;
import br.com.six2six.fixturefactory.Rule;
import br.com.six2six.fixturefactory.loader.TemplateLoader;

public class AtivoTemplateLoader implements TemplateLoader {

    public static final String RANDOM = "RANDOM";

    public static final String PETR4 = "PETR4";

    @Override
    public void load() {
        Fixture.of(Ativo.class).addTemplate(RANDOM, new Rule() {
            {
                add("codigo", firstName());
                add("descricao", name());
                add("emissor", lastName());
            }
        });

        Fixture.of(Ativo.class).addTemplate(PETR4, new Rule() {
            {
                add("codigo", "PETR4");
                add("descricao", "PN N2");
                add("emissor", "PETROLEO BRASILEIRO S.A. PETROBRAS");
            }
        });
    }

}
