package br.com.itau.instrumentos.apicadastro.core.usecase;

import br.com.itau.instrumentos.apicadastro.core.entity.Ativo;
import br.com.itau.instrumentos.apicadastro.core.usecase.dto.AtivoResponse;
import br.com.itau.instrumentos.apicadastro.core.usecase.gateway.BuscarPorCodigoAtivoGateway;
import br.com.itau.instrumentos.apicadastro.template.AtivoTemplateLoader;
import br.com.itau.instrumentos.base.dto.response.ListaErroEnum;
import br.com.itau.instrumentos.base.gateway.DeletarGateway;
import br.com.six2six.fixturefactory.Fixture;
import br.com.six2six.fixturefactory.loader.FixtureFactoryLoader;
import org.junit.Assert;
import org.junit.BeforeClass;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.Mockito;
import org.mockito.junit.MockitoJUnitRunner;

import java.util.Optional;

@RunWith(MockitoJUnitRunner.class)
public class DeletarAtivoUseCaseImplTest {

    @InjectMocks
    private DeletarAtivoUseCaseImpl usecase;

    @Mock
    private DeletarGateway<String> gateway;

    @Mock
    private BuscarPorCodigoAtivoGateway buscarPorCodigoAtivoGateway;

    @BeforeClass
    public static void beforeClass() {
        FixtureFactoryLoader.loadTemplates("br.com.iitau.instrumentos.apicadastro.template");
    }

    @Test
    public void deve_deletar_ativo() {
        Ativo ativoPetr4 = Fixture.from(Ativo.class).gimme(AtivoTemplateLoader.PETR4);

        Mockito.when(buscarPorCodigoAtivoGateway.buscarPorCodigoAtivo(ativoPetr4.getCodigo())).thenReturn(Optional.of(ativoPetr4));
        Mockito.when(gateway.deletar(ativoPetr4.getCodigo())).thenReturn(true);

        AtivoResponse response = usecase.executar(ativoPetr4.getCodigo());

        Assert.assertEquals(response.getId(), ativoPetr4.getId());
        Assert.assertEquals(response.getCodigo(), ativoPetr4.getCodigo());
        Assert.assertEquals(response.getDescricao(), ativoPetr4.getDescricao());
        Assert.assertEquals(response.getEmissor(), ativoPetr4.getEmissor());
    }

    @Test
    public void nao_deve_deletar_ativo_inexistente() {
        Ativo ativoPetr4 = Fixture.from(Ativo.class).gimme(AtivoTemplateLoader.PETR4);

        Mockito.when(buscarPorCodigoAtivoGateway.buscarPorCodigoAtivo(ativoPetr4.getCodigo())).thenReturn(Optional.empty());

        AtivoResponse response = usecase.executar(ativoPetr4.getCodigo());

        Assert.assertNull(response.getId());
        Assert.assertEquals(response.getCodigo(), ativoPetr4.getCodigo());
        Assert.assertNull(response.getDescricao());

        Assert.assertEquals(1, response.getResponse().getErros().size());
        Assert.assertEquals(ListaErroEnum.ENTIDADE_NAO_ENCONTRADA, response.getResponse().getErros().get(0).getTipo());
    }

    @Test
    public void nao_foi_possivel_deletar() {
        Ativo ativoPetr4 = Fixture.from(Ativo.class).gimme(AtivoTemplateLoader.PETR4);

        Mockito.when(buscarPorCodigoAtivoGateway.buscarPorCodigoAtivo(ativoPetr4.getCodigo())).thenReturn(Optional.of(ativoPetr4));

        Mockito.when(gateway.deletar(ativoPetr4.getCodigo())).thenReturn(false);

        AtivoResponse response = usecase.executar(ativoPetr4.getCodigo());

        Assert.assertNull(response.getId());
        Assert.assertEquals(response.getCodigo(), ativoPetr4.getCodigo());
        Assert.assertNull(response.getDescricao());
        Assert.assertNull(response.getEmissor());
        Assert.assertEquals(1, response.getResponse().getErros().size());
        Assert.assertEquals(ListaErroEnum.NAO_FOI_POSSIVEL_DELETAR, response.getResponse().getErros().get(0).getTipo());
    }
}
